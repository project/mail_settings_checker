Description
-----------
This module was created in order to alleviate some of the issues related to the mailsystem settings getting automatically reset due to losing access to the mailsystem file. Please see https://www.drupal.org/node/2832447 for the issue discussed.

Basically if the mailsystem file (sites/default/files/mailsystem/MimeMailSystem__SmtpMailSystem.mail.inc) becomes unavailable temporarily and the website tries to send a mail within this time, it will end up resetting the site variables back to default mailsystem, preventing mails from being properly sent.

Currently this module assumes that the mailsystem file is located at : sites/default/files/mailsystem/MimeMailSystem__SmtpMailSystem.mail.inc

Installation
------------
1) Place the module folder in your sites/all/modules folder
2) Enable the module at admin/modules
3) Visit admin/config/system/mail_settings_checker to change the desired settings
4) See usage section on receiving emails.

Usage
-----
The cron task will check the system variable every minute, this is done via hook_cron. If a change is detected it will attempt to fix it to the
variable assignments that you have set in the configuration page. This is important to note, as if you desire to later change the mail system settings
provided by the mailsystem module at (admin/config/system/mailsystem) you must make sure to also update them at admin/config/system/mail_settings_checker or they will be overwritten.
Easiest way to update them is to simply remove all the content from the Variable field, hit save, and it will pick whatever is found at admin/config/system/mailsystem.

If smtp is used and the the mailsystem file file becomes unavailable then any emails sent will most likely go to your spam folder.
To have the best effect, you should create a filter in your mail provider to mark these as non spam. This way you will receive proper notification
even if the mail system file becomes unavailable.






